const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const writeStream = fs.createWriteStream('post.json');

// Write header
writeStream.write(`Title, List w.links\n`);

request('https://rammwiki.net/wiki/Rammstein_Biography', (error, response, html) => {
    if (!error && response.statusCode == 200) {
        const getter = cheerio.load(html);

        const firstHeading = getter('.firstHeading');
        //console.log(firstHeading.html());
        
        getter('.portal li').each((i, el) => {
            const item = getter(el).text();
            const link = getter(el).find('a').attr('href');

            //console.log(item, link);
            // Write to JSON
            writeStream.write(`List Item: ${item} Link: ${link}\n`);
        });
        
        console.log('Scraping Done...');
    }
});